package mockfyne

import (
	"fyne.io/fyne/v2"
	"github.com/stretchr/testify/mock"
)

type MockWindow struct {
	mock.Mock
}

func (m *MockWindow) Show() {
	m.Called()
}

func (m *MockWindow) Canvas() fyne.Canvas {
	return &MockCanvas{}
}

func (m *MockWindow) CenterOnScreen() {
	m.Called()
}

func (m *MockWindow) Clipboard() fyne.Clipboard {
	args := m.Called()
	return args.Get(0).(fyne.Clipboard)
}

func (m *MockWindow) Close() {
	m.Called()
}

func (m *MockWindow) Content() fyne.CanvasObject {
	args := m.Called()
	return args.Get(0).(fyne.CanvasObject)
}

func (m *MockWindow) FixedSize() bool {
	args := m.Called()
	return args.Get(0).(bool)
}

func (m *MockWindow) FullScreen() bool {
	args := m.Called()
	return args.Get(0).(bool)
}

func (m *MockWindow) Hide() {
	m.Called()
}

func (m *MockWindow) Icon() fyne.Resource {
	args := m.Called()
	return args.Get(0).(fyne.Resource)
}

func (m *MockWindow) Invalidate() {
	m.Called()
}

func (m *MockWindow) MainMenu() *fyne.MainMenu {
	args := m.Called()
	return args.Get(0).(*fyne.MainMenu)
}

func (m *MockWindow) Maximize() {
	m.Called()
}

func (m *MockWindow) MinSize() fyne.Size {
	args := m.Called()
	return args.Get(0).(fyne.Size)
}

func (m *MockWindow) Padded() bool {
	args := m.Called()
	return args.Get(0).(bool)
}

func (m *MockWindow) Position() fyne.Position {
	args := m.Called()
	return args.Get(0).(fyne.Position)
}

func (m *MockWindow) RequestFocus() {
	m.Called()
}

func (m *MockWindow) Resize(size fyne.Size) {
	m.Called(size)
}

func (m *MockWindow) SetCloseIntercept(onClose func()) {
	m.Called(onClose)
}

func (m *MockWindow) SetContent(content fyne.CanvasObject) {
	m.Called(content)
}

func (m *MockWindow) SetFixedSize(fixed bool) {
	m.Called(fixed)
}

func (m *MockWindow) SetFullScreen(fullscreen bool) {
	m.Called(fullscreen)
}

func (m *MockWindow) SetIcon(resource fyne.Resource) {
	m.Called(resource)
}

func (m *MockWindow) SetMainMenu(menu *fyne.MainMenu) {
	m.Called(menu)
}

func (m *MockWindow) SetMaster() {
	m.Called()
}

func (m *MockWindow) SetOnClosed(closed func()) {
	m.Called(closed)
}

func (m *MockWindow) SetPadded(padded bool) {
	m.Called(padded)
}

func (m *MockWindow) SetPosition(position fyne.Position) {
	m.Called(position)
}

func (m *MockWindow) SetResizable(resizable bool) {
	m.Called(resizable)
}

func (m *MockWindow) SetSize(size fyne.Size) {
	m.Called(size)
}

func (m *MockWindow) SetTitle(title string) {
	m.Called(title)
}

func (m *MockWindow) ShowAndRun() {
	m.Called()
}

func (m *MockWindow) Size() fyne.Size {
	args := m.Called()
	return args.Get(0).(fyne.Size)
}

func (m *MockWindow) Title() string {
	args := m.Called()
	return args.Get(0).(string)
}

func (m *MockWindow) UnFullScreen() {
	m.Called()
}

func (m *MockWindow) Visible() bool {
	args := m.Called()
	return args.Get(0).(bool)
}
