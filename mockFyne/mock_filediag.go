package mockfyne

import "github.com/stretchr/testify/mock"

type MockFileDialog struct {
	mock.Mock
}

func (m *MockFileDialog) Show() {
	m.Called()
}
