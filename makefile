current_dir := $(shell pwd)

build:
	go build -ldflags="-s -w"
	upx --brute sgt

build-dev:
	go get -u ./...
	go build

build-containerized:
	docker run -v "$(current_dir):/build" \
	-w /build golang:alpine \
	sh -c 'apk add --no-cache upx git build-base pkgconf \
			mesa-dev libx11-dev libxcursor-dev libxrandr-dev \
			libxinerama-dev libxi-dev && \
		go build -ldflags="-s -w" && \
		upx --brute sgt'

install:
	install sgt /usr/bin/

install-user:
	install sgt ~/.local/bin

test:
	go test ./...

test-containerized:
	docker run -v "$(current_dir):/build" \
	-w /build golang:alpine \
	sh -c 'apk add --no-cache upx git build-base pkgconf \
			mesa-dev libx11-dev libxcursor-dev libxrandr-dev \
			libxinerama-dev libxi-dev dbus dbus-libs && \
		export DBUS_SESSION_BUS_ADDRESS=`dbus-daemon --fork \
			--config-file=/usr/share/dbus-1/session.conf --print-address` && \
		go test -v ./... -coverprofile coverage.out -timeout 30s'

flatpak:
	flatpak-builder --repo=./flatpak-repo --force-clean ./flatpak flatpak.yml

flatpak-install:
	flatpak-builder --user --install --force-clean ./flatpak flatpak.yml

flatpak-run:
	flatpak run net.cyphergaming.sgt
