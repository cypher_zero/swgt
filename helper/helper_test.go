package helper

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrettyMap(t *testing.T) {
	// Sample input map
	inputMap := map[string]interface{}{
		"key1": "value1",
		"key2": 123,
		"key3": []string{"a", "b", "c"},
	}

	// Expected pretty-printed JSON
	expectedJSON := `{
  "key1": "value1",
  "key2": 123,
  "key3": [
    "a",
    "b",
    "c"
  ]
}`

	// Call the PrettyMap function
	result, err := PrettyMap(inputMap)

	// Ensure no error occurred during marshaling
	assert.NoError(t, err)

	// Compare the result with the expected JSON
	assert.Equal(t, expectedJSON, string(result))
}

func TestRandInt(t *testing.T) {
	// Test with i = 10
	result := RandInt(10)

	// Ensure the result is between 1 and 10 (inclusive)
	assert.GreaterOrEqual(t, result, 1)
	assert.LessOrEqual(t, result, 10)
}

func TestCleanString(t *testing.T) {
	// Test with an input string containing whitespace and punctuation
	input := "Hello, World. 123-456-798_0!@#$%^&*()[]{};:"
	expectedOutput := "hello_world._123-456-798_0"

	output := CleanString(input)

	// Ensure the output string is cleaned up as expected
	assert.Equal(t, expectedOutput, output)
}

func TestConvertSliceToString(t *testing.T) {
	// Test with an input slice containing multiple integers
	inputSlice := []int{1, 2, 3, 4, 5}
	expectedOutput := "1\n2\n3\n4\n5"

	output := ConvertSliceToString(&inputSlice)

	// Ensure the output string is generated correctly
	assert.Equal(t, expectedOutput, output)
}
