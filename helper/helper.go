// Semi-generic helper functions referenced by other modules
package helper

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"strings"
	"unicode"
)

// Takes a map and uses json.MarshalIndent to produce a pretty-printable []byte for
// logs, stdout, or other human-readable needs
func PrettyMap(v any) (out []byte, err error) {
	out, err = json.MarshalIndent(v, "", "  ")
	return out, err
}

// Returns a truly random number 1 to i
// Ref: https://yourbasic.org/golang/crypto-rand-int/
func RandInt(i int) int {
	nBig, err := rand.Int(rand.Reader, big.NewInt(int64(i)))
	if err != nil {
		return 0
	}

	return int(nBig.Int64() + 1)
}

// Cleans up the provided string, making it lowercase, replacing whitespace with
// underscores, and replacing any punctuation with periods
func CleanString(input string) (outStr string) {
	// Convert the string to lowercase
	outStr = strings.ToLower(input)

	// Remove punctuation except periods
	var sb strings.Builder
	for _, r := range outStr {
		if unicode.IsLetter(r) || unicode.IsDigit(r) || unicode.IsSpace(r) ||
			r == '.' || r == '-' || r == '_' {
			sb.WriteRune(r)
		}
	}
	outStr = sb.String()

	// Replace whitespace with underscores
	outStr = strings.ReplaceAll(outStr, " ", "_")

	return outStr
}

// Take a pointer to a an int slice and converts it into a string
// with one int value per line
func ConvertSliceToString(slice *[]int) string {
	var lines []string
	for _, value := range *slice {
		lines = append(lines, fmt.Sprint(value))
	}
	return strings.Join(lines, "\n")
}
