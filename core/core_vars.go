// Global variables for the core app

package core

var dataStor = coreData{
	Rollers: make(map[int]*rollerInfo),
	Stats:   make(map[string]*int),
}

var loadOverwrite bool
