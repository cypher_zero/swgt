package core

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type keyItems struct {
	notes     *widget.Entry
	gameField *widget.Entry
	autosave  *widget.Check
	statsBox  *fyne.Container
	rollerBox *fyne.Container
}

type rollerInfo struct {
	DVal    *int
	History *[]int
}

type coreData struct {
	Game     *string             `json:"game" yaml:"game"`
	SavePath string              `json:"savePath" yaml:"savePath"`
	Autosave bool                `json:"autosave" yaml:"autosave"`
	Notes    string              `json:"notes" yaml:"notes"`
	Stats    map[string]*int     `json:"stats" yaml:"stats"`
	Rollers  map[int]*rollerInfo `json:"rollers" yaml:"rollers"`
}
