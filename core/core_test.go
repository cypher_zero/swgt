package core

import (
	"os"
	"path/filepath"
	"testing"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/widget"

	"github.com/stretchr/testify/assert"

	"gopkg.in/yaml.v3"
)

func TestResetApp(t *testing.T) {
	// Initialize the Fyne application
	myApp := app.New()

	// Create mock objects for testing
	mainItems := &keyItems{
		notes:     &widget.Entry{},
		gameField: &widget.Entry{},
		autosave:  &widget.Check{},
		statsBox:  &fyne.Container{},
		rollerBox: &fyne.Container{},
	}

	// Set initial values for the mock objects
	mainItems.notes.Text = "Initial notes"
	mainItems.gameField.Text = "Initial game field"
	mainItems.autosave.Checked = true

	// Call the function to be tested
	resetApp(mainItems)

	// Assert that the mock objects are reset to their expected values
	assert.Equal(t, "", mainItems.notes.Text)
	assert.Equal(t, "", mainItems.gameField.Text)
	assert.Equal(t, false, mainItems.autosave.Checked)
	assert.Empty(t, mainItems.statsBox.Objects)
	assert.Empty(t, mainItems.rollerBox.Objects)

	// Quit the Fyne application
	myApp.Quit()
}

// func TestCreateExitBtn

// func TestCreateNewDocBtn

// func TestCreateAutosaveChk

// func TestSetSaveFilename

func TestSetSaveFilename(t *testing.T) {
	// Test when dataStor.Game is not nil
	dataStor.Game = new(string)
	*dataStor.Game = "My Game"
	expectedFilename := "my_game.yml"
	actualFilename := setSaveFilename()
	assert.Equal(t, expectedFilename, actualFilename)

	// Test when dataStor.Game is nil
	dataStor.Game = nil
	expectedFilename = "game.yml"
	actualFilename = setSaveFilename()
	assert.Equal(t, expectedFilename, actualFilename)
}

// func TestCreateSaveBtn

// func TestSaveDialog

func TestSaveDataToFile(t *testing.T) {
	// Define test data
	data := map[string]string{
		"key1": "value1",
		"key2": "value2",
	}
	homedir, err := os.UserHomeDir()
	assert.NoError(t, err)

	filePath := filepath.Join(homedir, "/test_file.yml")

	// Call the function
	err = saveDataToFile(data, filePath)
	defer os.Remove(filePath)

	// Verify there are no errors
	assert.NoError(t, err)

	// Read the file and unmarshal YAML data
	fileData, err := os.ReadFile(filePath)
	assert.NoError(t, err)

	var unmarshaledData map[string]string
	err = yaml.Unmarshal(fileData, &unmarshaledData)
	assert.NoError(t, err)

	// Verify the unmarshaled data matches the original data
	assert.Equal(t, data, unmarshaledData)
}

// func tempCanvasCall(canvas fyne.Canvas) {}
