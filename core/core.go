package core

import (
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/cypher_zero/sgt/helper"
	"gopkg.in/yaml.v3"
)

// The main function call to start the core app
func Execute() {
	var autosaveRunning bool
	var autosaveDone chan bool

	mainApp := app.New()
	mainWin := mainApp.NewWindow("Simple Game Tracker")

	gameNameLabel := widget.NewLabel("Game:")
	gameNameLabel.TextStyle.Bold = true
	gameNameEntry := widget.NewEntry()
	gameNameEntry.PlaceHolder = "Game name"
	gameNameEntry.OnChanged = func(text string) {
		dataStor.Game = &text
	}

	exitBtn := createExitBtn(mainWin, mainApp)

	spacer := layout.NewSpacer()

	autosaveChk := createAutosaveChk(mainWin, &autosaveRunning, autosaveDone)

	statsLabel := widget.NewLabel("Stats:")
	statsLabel.TextStyle.Bold = true
	statsBox := container.NewGridWrap(fyne.NewSize(178, exitBtn.MinSize().Height))
	addStatBtn := addStat(statsBox)

	statsBoxWrapper := container.NewBorder(nil, nil, addStatBtn, nil, addStatBtn, statsBox)
	statsHideChk := hiderChk(statsBoxWrapper)

	rollerLabel := widget.NewLabel("Rollers:")
	rollerLabel.TextStyle.Bold = true
	rollerBox := container.NewGridWrap(fyne.NewSize(132, 76))
	addRollerBtn := addRoller(rollerBox)
	rollerBoxWrapper := container.NewBorder(
		nil, nil, addRollerBtn, nil, addRollerBtn, rollerBox, spacer,
	)

	rollersHideChk := hiderChk(rollerBoxWrapper)

	rollerWrapper := container.NewVBox(
		container.NewHBox(rollerLabel, spacer, rollersHideChk),
		rollerBoxWrapper,
	)

	notesLabel := widget.NewLabel("Notes:")
	notesLabel.TextStyle.Bold = true

	notesField := widget.NewMultiLineEntry()
	notesField.SetPlaceHolder("Type here...")
	notesField.Wrapping = fyne.TextWrapWord
	notesField.OnChanged = func(text string) {
		dataStor.Notes = strings.TrimSpace(text)
	}

	notesFieldWrapper := container.NewBorder(nil, nil, nil, nil, notesField)
	notesHideChk := hiderChk(notesFieldWrapper)

	mainItems := keyItems{
		notes:     notesField,
		gameField: gameNameEntry,
		autosave:  autosaveChk,
		statsBox:  statsBox,
		rollerBox: rollerBox,
	}

	newBtn := createNewDocBtn(mainWin, &mainItems)
	saveBtn := createSaveBtn(mainWin)
	openBtn := createOpenBtn(mainWin, &mainItems, addRollerBtn)

	topBar := container.NewHBox(
		newBtn,
		openBtn,
		saveBtn,
		spacer,
		exitBtn,
	)

	mainContent := container.NewVBox(
		topBar,
		container.NewBorder(
			nil, nil, gameNameLabel, autosaveChk, gameNameLabel, gameNameEntry, autosaveChk,
		),
		container.NewHBox(statsLabel, spacer, statsHideChk),
		statsBoxWrapper,
		rollerWrapper,
		container.NewHBox(notesLabel, spacer, notesHideChk),
	)

	content := container.NewBorder(
		mainContent, nil, nil, nil,
		mainContent,
		notesFieldWrapper,
	)

	mainWin.SetContent(content)
	// Set the initial size of the window
	mainWin.Resize(fyne.NewSize(600, 600))
	mainWin.ShowAndRun()
}

func resetApp(mainItems *keyItems) {
	mainItems.notes.Text = ""
	mainItems.notes.Refresh()

	mainItems.gameField.Text = ""
	mainItems.gameField.Refresh()

	mainItems.autosave.Checked = false
	mainItems.autosave.Refresh()

	mainItems.statsBox.Objects = []fyne.CanvasObject{}
	mainItems.statsBox.Refresh()

	mainItems.rollerBox.Objects = []fyne.CanvasObject{}
	mainItems.rollerBox.Refresh()
}

func createExitBtn(win fyne.Window, app fyne.App) *widget.Button {
	return widget.NewButtonWithIcon("Exit", theme.CancelIcon(), func() {
		saveConfirm := dialog.NewConfirm("Save before quitting?", "Do you want to save before quitting?", func(confirmed bool) {
			if confirmed {
				// Show the save dialog
				saveFileDialog := saveDialog(win)
				saveFileDialog.SetFileName(setSaveFilename())
				saveFileDialog.Show()
				saveFileDialog.SetOnClosed(func() {
					dialog.ShowInformation("Exiting", "Exiting in 3 seconds.", win)
					go func() {
						time.Sleep(3 * time.Second)
						app.Quit()
					}()
				})
			} else {
				app.Quit()
			}
		}, win)

		saveConfirm.Show()
	})
}

func createNewDocBtn(win fyne.Window, mainItems *keyItems) *widget.Button {
	return widget.NewButtonWithIcon("New", theme.DocumentIcon(), func() {
		dialog.ShowConfirm("Confirmation", "Clear current data (saved files will be unaffected)?",
			func(confirmed bool) {
				if confirmed {
					resetApp(mainItems)
				}
			}, win)
	})
}

func createAutosaveChk(win fyne.Window, autosaveRunning *bool, autosaveDone chan bool) *widget.Check {
	return widget.NewCheck("Enable Autosave?", func(checked bool) {
		if checked { // Autosave enabled
			if len(strings.TrimSpace(dataStor.SavePath)) < 5 {
				// Show the save dialog
				saveFileDialog := saveDialog(win)
				saveFileDialog.SetFileName(setSaveFilename())
				saveFileDialog.Show()
				saveFileDialog.SetOnClosed(func() {
					dialog.ShowInformation("Autosave",
						"Autosave will automatically save the game file every 60 seconds.", win)
				})
			} else {
				dialog.ShowInformation("Autosave",
					"Autosave will automatically save the game file every 60 seconds.", win)
			}
			if !*autosaveRunning {
				autosaveDone = make(chan bool)
				go autosaveRoutine(autosaveDone)
				*autosaveRunning = true
			}

		} else { // Autosave disabled
			if *autosaveRunning {
				autosaveDone <- true
				*autosaveRunning = false
			}
		}
		dataStor.Autosave = checked
	})
}

func setSaveFilename() string {
	if dataStor.Game != nil {
		return helper.CleanString(*dataStor.Game) + ".yml"
	} else {
		return "game.yml"
	}
}

func createSaveBtn(win fyne.Window) *widget.Button {
	return widget.NewButtonWithIcon("Save", theme.DocumentSaveIcon(), func() {
		saveFileDialog := saveDialog(win)
		saveFileDialog.SetFileName(setSaveFilename())

		// Show the save dialog
		saveFileDialog.Show()
	})
}

func saveDialog(win fyne.Window) *dialog.FileDialog {
	return dialog.NewFileSave(func(fileURL fyne.URIWriteCloser, err error) {
		if err != nil {
			return
		}
		if fileURL == nil {
			return
		}

		// Retrieve the file path from the fileURL
		dataStor.SavePath = fileURL.URI().Path()

		// Close the fileURL when done
		defer fileURL.Close()

		err = saveDataToFile(dataStor, dataStor.SavePath)
		if err != nil {
			// Handle error
			fmt.Println("Error saving data:", err)
			return
		}
	}, win)
}

// Function to save dataStor as a YAML file
func saveDataToFile(data interface{}, filePath string) error {
	// Marshal dataStor to YAML format
	yamlData, err := yaml.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to marshal data to YAML: %v", err)
	}

	basePath, err := os.UserHomeDir()
	if err != nil {
		return fmt.Errorf("failed parse home directory: %v", err)
	}
	if !strings.HasPrefix(filePath, basePath) {
		return errors.New("path does not start with basePath")
	}

	// Create the file if it doesn't exist
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return fmt.Errorf("failed to create file: %v", err)
	}
	defer file.Close()

	// Write YAML data to the file
	_, err = file.Write(yamlData)
	if err != nil {
		return fmt.Errorf("failed to write data to file: %v", err)
	}

	return nil
}

func createOpenBtn(win fyne.Window, mainItems *keyItems, addRollerBtn *widget.Button) *widget.Button {
	return widget.NewButtonWithIcon("Open", theme.FolderOpenIcon(), func() {
		openFileDialog := openDialog(win, mainItems, addRollerBtn)
		openFileDialog.Show()
	})
}

func openDialog(win fyne.Window, mainItems *keyItems, addRollerBtn *widget.Button) *dialog.FileDialog {
	return dialog.NewFileOpen(func(fileURL fyne.URIReadCloser, err error) {
		if err != nil {
			return
		}
		if fileURL == nil {
			return
		}

		// Read the contents of the file
		fileContents, err := io.ReadAll(fileURL)
		if err != nil {
			dialog.ShowError(err, win)
			return
		}

		// Close the fileURL when done
		fileURL.Close()

		// Unmarshal the YAML data into dataStor
		err = yaml.Unmarshal(fileContents, &dataStor)
		if err != nil {
			dialog.ShowError(err, win)
			return
		}

		resetApp(mainItems)

		loadOverwrite = true

		mainItems.notes.Text = dataStor.Notes
		mainItems.notes.Refresh()

		mainItems.gameField.Text = *dataStor.Game
		mainItems.gameField.Refresh()

		mainItems.autosave.SetChecked(dataStor.Autosave)
		// mainItems.autosave.Tapped(&fyne.PointEvent{})

		// Clear existing containers in statsBox
		mainItems.statsBox.Objects = []fyne.CanvasObject{}
		mainItems.statsBox.Refresh()

		// Create new containers for each stat
		for statName := range dataStor.Stats {
			valLabelEntry := widget.NewEntry()
			valLabelEntry.SetText(statName)
			valBox := createValBox(mainItems.statsBox, valLabelEntry)

			// Trigger the updateValBox function to set up the layout
			valBox.Objects[0].(*widget.Button).OnTapped()
			mainItems.statsBox.Add(valBox)
		}
		mainItems.statsBox.Refresh()

		// Clear existing containers in rollerBox
		mainItems.rollerBox.Objects = []fyne.CanvasObject{}
		mainItems.rollerBox.Refresh()

		// Create new containers for each roller
		for range dataStor.Rollers {
			addRollerBtn.OnTapped()
		}

		loadOverwrite = false

	}, win)
}

// Saves the file automatically every 60 seconds; called as a go routine
func autosaveRoutine(done chan bool) {
	ticker := time.NewTicker(60 * time.Second)
	for {
		select {
		case <-done:
			ticker.Stop()
			return
		case <-ticker.C:
			saveDataToFile(dataStor, dataStor.SavePath)
		}
	}
}

func hiderChk(hideContainer *fyne.Container) *widget.Check {
	return widget.NewCheck("Hide?", func(checked bool) {
		if checked {
			hideContainer.Hide()
		} else {
			hideContainer.Show()
		}
	})
}

func updateStatInfo(title string, valStr *string) {
	statVal, err := strconv.Atoi(*valStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	dataStor.Stats[title] = &statVal
}

func addStat(widgets *fyne.Container) *widget.Button {
	return widget.NewButtonWithIcon("", theme.ContentAddIcon(), func() {
		valLabelEntry := widget.NewEntry()
		valLabelEntry.Text = "Stat Name"
		valBox := createValBox(widgets, valLabelEntry)
		widgets.Add(valBox)
	})
}

func createValBox(widgets *fyne.Container, valLabelEntry *widget.Entry) *fyne.Container {
	labelSave := widget.NewButtonWithIcon("", theme.ConfirmIcon(), nil)
	valBox := container.NewBorder(
		nil, nil, nil, labelSave,
		labelSave,
		valLabelEntry,
	)

	updateValBox := func() {
		trimmedLabel := strings.TrimSpace(valLabelEntry.Text)
		// Prevent creation of duplicate stats
		for l := range dataStor.Stats {
			if l == trimmedLabel && !loadOverwrite {
				return
			}
		}
		valLabel := widget.NewLabel(trimmedLabel + ":")
		valTracker := widget.NewEntry()
		if dataStor.Stats[trimmedLabel] == nil {
			dataStor.Stats[trimmedLabel] = new(int)
		}
		valTracker.Text = fmt.Sprint(*dataStor.Stats[trimmedLabel])

		valTracker.OnChanged = func(text string) {
			updateStatInfo(trimmedLabel, &text)
		}

		upBtn := newUpBtn(valTracker, trimmedLabel)
		dwnBtn := newDwnBtn(valTracker, trimmedLabel)

		valButtonsWrapper := container.NewGridWithColumns(1,
			upBtn,
			dwnBtn,
		)

		spacer := layout.NewSpacer()

		valBox.Layout = layout.NewHBoxLayout()
		valBox.Objects = []fyne.CanvasObject{spacer, valLabel, valTracker, valButtonsWrapper}
		valBox.Refresh()
	}

	labelSave.OnTapped = updateValBox

	valLabelEntry.OnSubmitted = func(text string) {
		updateValBox()
	}

	return valBox
}

func newUpBtn(w *widget.Entry, title string) *widget.Button {
	return widget.NewButton("+", func() {
		sInt, err := strconv.Atoi(w.Text)
		if err == nil {
			sInt++
			w.Text = fmt.Sprint(sInt)
			w.Refresh()
			updateStatInfo(title, &w.Text)
		}
	})
}

func newDwnBtn(w *widget.Entry, title string) *widget.Button {
	return widget.NewButton("-", func() {
		sInt, err := strconv.Atoi(w.Text)
		if err == nil {
			sInt--
			w.Text = fmt.Sprint(sInt)
			w.Refresh()
			updateStatInfo(title, &w.Text)
		}
	})
}

func addRoller(widgets *fyne.Container) *widget.Button {
	return widget.NewButtonWithIcon("", theme.ContentAddIcon(), func() {
		rollerIndex := len(widgets.Objects)
		var dVal int

		if dataStor.Rollers[rollerIndex] == nil {
			dataStor.Rollers[rollerIndex] = &rollerInfo{}
		}

		if dataStor.Rollers[rollerIndex].DVal == nil {
			dVal = 6
		} else {
			dVal = *dataStor.Rollers[rollerIndex].DVal
		}
		var history []int
		if dataStor.Rollers[rollerIndex].History == nil {
			history = []int{}
		} else {
			history = *dataStor.Rollers[rollerIndex].History
		}
		rollerData := &rollerInfo{
			DVal:    &dVal,
			History: &history,
		}
		dataStor.Rollers[rollerIndex] = rollerData

		rollOut := widget.NewMultiLineEntry()
		rollOut.Wrapping = fyne.TextWrapBreak
		rollOut.TextStyle.Bold = true
		rollOut.Text = helper.ConvertSliceToString(&history)
		rollOut.OnCursorChanged = func() {
			rollOut.Text = helper.ConvertSliceToString(&history)
		}

		btnLabel := widget.NewLabel("d")

		btnLabelEntry := widget.NewEntry()
		btnLabelEntry.Text = fmt.Sprint(dVal)

		btn := rollBtn(&dVal, &history, rollOut)
		btnLabelEntry.OnChanged = func(text string) {
			// Create a regular expression pattern to match non-digit characters
			pattern := "[^0-9]+"
			re := regexp.MustCompile(pattern)

			// Use the regular expression to replace non-digit characters with an empty string
			text = re.ReplaceAllString(text, "")
			textInt, _ := strconv.Atoi(text)
			if text == "" || textInt < 2 {
				text = "6"
			} else {
				dVal = textInt
			}
			btnLabelEntry.Text = text
			btn.SetText("Roll d" + text)
		}

		spacer := layout.NewSpacer()

		btnDetsWrapper := container.NewHBox(
			container.NewBorder(nil, nil, spacer, nil, spacer, btnLabel),
			btnLabelEntry,
		)

		btnWrapper := container.NewVBox(
			btn,
			btnDetsWrapper,
		)
		rollerInnerWrapper := container.NewHBox(
			btnWrapper,
			rollOut,
		)
		widgets.Add(rollerInnerWrapper)
	})
}

func rollBtn(dX *int, history *[]int, rollOut *widget.Entry) *widget.Button {
	return widget.NewButton("Roll d"+fmt.Sprint(*dX), func() {
		rndResult := helper.RandInt(*dX)
		if len(*history) > 99 {
			*history = append([]int{rndResult}, (*history)[:98]...)
		} else {
			*history = append([]int{rndResult}, *history...)
		}

		rollOut.Text = helper.ConvertSliceToString(history)
		rollOut.Refresh()
	})
}
