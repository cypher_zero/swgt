# Simple Game Tracker (sgt)

A simple app for tracking simple RPG type games.

![sgt screenshot](./sgt_screenshot.webp)

## Key Features

- A simple UI
- Dynamic stat tracking
- Dynamic die rollers with stored history (99 rolls)
- Large notes section
- Save and restore data as YAML (.yml) files
- Toggleable autosave (every 60 seconds)

## Quick Install Steps

TL;DR:

```bash
git clone https://gitlab.com/cypher_zero/sgt.git
cd sgt
make build-containerized # Requires docker or podman + podman-docker
make install-user
```

## Building

### Binary - Recommended

The recommended way to build is to use `make build-containerized`.
This will build `sgt` in the current directory using `docker`
  (or `podman` if `podman-docker` is also installed).

### Binary - Alternative

Alternatively, use `make build` to build the package locally.
This requires at least the following packages be installed locally:

- `go`
- `upx`
- (probably others)

### Flatpak

Once you have built the binary, you can use `make flatpak` to build the package into a flatpak.
If you want to install sgt directly from there, use `make flatpak-install` instead.

## Installing

Once the binary (`sgt`) is built, it can be installed using one of the following methods.

Note that this just installs the binary so that it can be run without referencing it directly;
  if you want a shortcut in your GUI app launcher, you'll need to create your own (for now).

### Installing from Flathub (WIP)

`sgt` is not yet available in Flathub, but we hope to be soon!

### Flatpak Install (Recommended)

This will install `sgt` as a flatpak for your current user.

Run: `make flatpak-install`

You can then run `sgt` with: `flatpak run net.cyphergaming.sgt`

### User Install

This will install `sgt` to your local user account under `~/.local/bin`.

Run: `make install-user`

To execute `sgt` from the cli, `~/.local/bin` must be in your path, otherwise call it
  directly with `~/.local/bin`.

### System-Wide Install (NOT Recommended)

This will install `sgt` system-wide, allowing all users to access it.

Run: `sudo make install`

This is not recommended as `sgt` does not need or use any resources outside of its own binary,
  except to save/load files.
